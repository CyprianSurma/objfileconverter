/*
 * FaceList.h
 *
 *  Created on: 21 lut 2017
 *      Author: Admin
 */

//list contains indices of vertex
#ifndef VERTEXLIST_H_
#define VERTEXLIST_H_
#include "Vector3dINT.h"
#include "defines.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
typedef Vector3dINT type;

typedef struct el
{
    type element;
	struct el * pnext;
}el;

typedef struct VertexList
{
	el * pnext;
	size_t size;
}VertexList;

int VertexList_push(VertexList*,type);
void VertexList_clear(VertexList*);
VertexList* VertexList_new();
void VertexList_SaveFacetoFile(VertexList*,FILE*);

#endif
