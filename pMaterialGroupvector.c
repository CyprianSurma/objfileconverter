


#include "pMaterialGroupvector.h"
#include <stdio.h>
#include <stdlib.h>
#include "defines.h"


pMaterialGroup_vector* pMaterialGroup_vector_new()
{
	pMaterialGroup_vector*p=malloc(sizeof(pMaterialGroup_vector));
	if(p)
	{
		p->array_size=STEP;
		p->size=0;
		p->vec=malloc(STEP*sizeof(MaterialGroup*));
		if(!p->vec)
		{
			free(p);
			p=NULL;
		}
	}
	return p;
}
int pMaterialGroup_vector_push_back(pMaterialGroup_vector*pvector,MaterialGroup*pmaterialgroup)
{
	if(pvector->size < pvector->array_size)
	{
		pvector->vec[(pvector->size)]=pmaterialgroup;
		pvector->size++;

	}
	else
	{
		pvector->vec=realloc(pvector->vec,(STEP+(pvector->array_size))*sizeof(MaterialGroup*));
		pvector->array_size+=STEP;
        if(pvector->vec)
        {
        	pvector->vec[(pvector->size)-1]=pmaterialgroup;
        	pvector->size++;
        }
        else
        {
        	return FUNCTION_FAILED;
        }

	}
	return FUNCTION_SUCCEDED;
}
int pMaterialGroup_vector_size(pMaterialGroup_vector*pvector)
{
	return pvector->size;
}


