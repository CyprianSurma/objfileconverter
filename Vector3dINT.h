/*
 * Vector3dINT.h
 *
 *  Created on: 21 lut 2017
 *      Author: Admin
 */
#include "defines.h"
#ifndef VECTOR3DINT_H_
#define VECTOR3DINT_H_

typedef struct
{
	int              v
					,vn
					,vt;
}Vector3dINT;

#endif /* VECTOR3DINT_H_ */
