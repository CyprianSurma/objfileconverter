
#include "MaterialGroup.h"


#ifndef PMATERIALGROUP_VECTOR_H_
#define PMATERIALGROUP_VECTOR_H_
#define STEP 2
typedef struct
{
	int size,array_size;
	MaterialGroup** vec;
}pMaterialGroup_vector;

pMaterialGroup_vector* pMaterialGroup_vector_new();

int pMaterialGroup_vector_push_back(pMaterialGroup_vector*,MaterialGroup*);

int pMaterialGroup_vector_size(pMaterialGroup_vector*);


#endif /* PMARTERIALGROUP_VECTOR_H_ */
