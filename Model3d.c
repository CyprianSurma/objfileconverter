/*
 * Model3d.c
 *
 *  Created on: 25 lut 2017
 *      Author: Admin
 */
#include  "Model3d.h"

void Model3d_clear(Model3d* pmodel)
{
	if(pmodel->vertices)
	{
		free(pmodel->vertices);
        pmodel->vertices=NULL;
	}
	if(pmodel->vertex_normals)
		{
			free(pmodel->vertex_normals);
	        pmodel->vertex_normals=NULL;
		}
	if(pmodel->texture_vertices)
			{
				free(pmodel->texture_vertices);
		        pmodel->texture_vertices=NULL;
			}
	if(pmodel->materials)
	{
		int i;
		for(i=0;i<pmodel->MaterialsCount;++i)
		{
			if(pmodel->materials[i])
			{
				free(pmodel->materials[i]);
				pmodel->materials[i]=NULL;
			}
		}
		free(pmodel->materials);
		pmodel->materials=NULL;
	}
	if(pmodel->groups)
	{

		int i;
		for(i=0;i<pmodel->GroupsCount;++i)
		{
			if(pmodel->groups[i])
			{
				group_clear(pmodel->groups[i]);
				free(pmodel->groups[i]);
			}
		}
		free(pmodel->groups);
	}
}
