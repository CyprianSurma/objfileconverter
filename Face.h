/*
 * Face.h
 *
 *  Created on: 23 lut 2017
 *      Author: Admin
 */

#ifndef FACE_H_
#define FACE_H_
#include "VertexList.h"
typedef struct
{
	VertexList*  vertices;
}Face;

Face* Face_new();
void Face_free(Face** );
int Face_addIndices(Face*,Vector3dINT);
#endif /* FACE_H_ */
