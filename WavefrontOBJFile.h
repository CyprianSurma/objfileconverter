#ifndef WAVEFRONT_OBJ_FILE_H_
#define  WAVEFRONT_OBJ_FILE_H_

#include "Model3d.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defines.h"
int ReadWavefronOBJfromfile(Model3d*,char*);
int SaveWavefrontOBJ(Model3d*,char*);
#endif
