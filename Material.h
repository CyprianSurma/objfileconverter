/*
 * Material.h
 *
 *  Created on: 25 lut 2017
 *      Author: Admin
 */

#ifndef MATERIAL_H_
#define MATERIAL_H_
#include "defines.h"
#include "Vector3d.h"

typedef struct
{
	Vector3d Ambientcolor
			,Diffusecolor
			,Specularcolor;
	float transparency
		 ,Ns;
	int Illuminationmodel;
	char  map_Ka[MAX_LENGHT_OF_NAME];                   // the ambient texture map
	char  map_Kd[MAX_LENGHT_OF_NAME];                   // the diffuse texture map (most of the time, it will be the same as the ambient texture map)
	char  map_Ks[MAX_LENGHT_OF_NAME];                   // specular color texture map
	char  map_Ns[MAX_LENGHT_OF_NAME]; 				    // specular highlight component
	char  map_d[MAX_LENGHT_OF_NAME];  			        // the alpha texture map
	char  map_bump[MAX_LENGHT_OF_NAME];			        // some implementations use 'map_bump' instead of 'bump' below
	char name[MAX_LENGHT_OF_NAME];
}Material;
Material* Material_new();

#endif /* MATERIAL_H_ */
