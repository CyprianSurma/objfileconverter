#ifndef MODEL_3D_H_
#define  MODEL_3D_H_
#include "Vector3d.h"
#include "defines.h"
#include "Group.h"
#include "Material.h"

typedef struct
{
	int NoGrups;        //FALSE if object have at least one grup
  	Vector3d  *vertices
			 ,*vertex_normals
			 ,*texture_vertices;

  	unsigned int VerticesCount
				,VertexNormalsCount
				,TextureVerticesCount
				,FacesCount
				,GroupsCount
  	            ,MaterialsCount;
   char materiallibname[MAX_LENGHT_OF_NAME];
   group **groups;
   Material** materials;
}Model3d;
void Model3d_clear(Model3d* pmodel);






#endif
