/*
 * FaceList.h
 *
 *  Created on: 21 lut 2017
 *      Author: Admin
 */

//head of list have to be initialized with 0
#ifndef FACELIST_H_
#define FACELIST_H_
#include "Vector3dINT.h"
#include "defines.h"
#include <string.h>
#include <stdlib.h>
#include "VertexList.h"
typedef VertexList typ;

typedef struct elem
{
    typ element;
	struct elem * pnext;
}elem;

typedef struct FaceList
{
	elem * pnext;
	size_t size;
}FaceList;

int  FaceList_push(FaceList*,typ);
void  FaceList_clear(FaceList*);
FaceList*  FaceList_new();
typ * FaceList_GetElement(FaceList*,size_t);
#endif /* FACELIST_H_ */
