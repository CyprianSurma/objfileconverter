/*
 * FaceList.c
 *
 *  Created on: 21 lut 2017
 *      Author: Admin
 */
#include "VertexList.h"
int VertexList_push(VertexList* head,type element)
{

	if(head->size>0)
	{
        el* node=head->pnext;
		while(node->pnext)
		{
			node=node->pnext;
		}
	    node->pnext=malloc(sizeof(el));
	    if(node->pnext==NULL)
	    {
	    	return FUNCTION_FAILED;
	    }
	     memset(node->pnext,0,sizeof(el));
	     node->pnext->element=element;
	}
	else
	{
		head->pnext=malloc(sizeof(el));
		if(head->pnext==NULL)
			    {
			    	return FUNCTION_FAILED;
			    }
			     memset(head->pnext,0,sizeof(el));
			     head->pnext->element=element;
	}



    head->size++;

    return FUNCTION_SUCCEDED;
}
void VertexList_clear(VertexList *l)
{
	el* pnext;
	size_t i;
	for (i=0;i<l->size;++i)
	{
		pnext=l->pnext;
		l->pnext=pnext->pnext;
		free(pnext);
	}
    l->size=0;
}
VertexList* VertexList_new()
{
	VertexList*l=malloc(sizeof(VertexList));
	if(l)
	{
	memset(l,0,sizeof(VertexList));
	}
	return l;
}
void VertexList_SaveFacetoFile(VertexList*plist,FILE*file)
{
	el* p=plist->pnext;
	int i=0;
	fprintf(file,"f ");
	for(i=0;i<plist->size;++i)
	{
		if(p->element.v>0)
		{
			fprintf(file,"%i",p->element.v);
		}
		if(p->element.vt>0)
		{
			fprintf(file,"/%i",p->element.vt);
			if(p->element.vn>0)
			{
				fprintf(file,"/%i",p->element.vn);
			}
		}
		else if(p->element.vn>0)
		{
			fprintf(file,"//%i",p->element.vn);
		}
		fprintf(file," ");
		p=p->pnext;
	}
	fprintf(file,"\n");
}
