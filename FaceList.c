/*
 * FaceList.c
 *
 *  Created on: 21 lut 2017
 *      Author: Admin
 */
#include "FaceList.h"
int  FaceList_push(FaceList* head,typ element)
{

	if(head->size>0)
	{
        elem* node=head->pnext;
		while(node->pnext)
		{
			node=node->pnext;
		}
	    node->pnext=malloc(sizeof(elem));
	    if(node->pnext==NULL)
	    {
	    	return FUNCTION_FAILED;
	    }
	     memset(node->pnext,0,sizeof(elem));
	     node->pnext->element=element;
	}
	else
	{
		head->pnext=malloc(sizeof(elem));
		if(head->pnext==NULL)
			    {
			    	return FUNCTION_FAILED;
			    }
			     memset(head->pnext,0,sizeof(elem));
			     head->pnext->element=element;
	}



    head->size++;

    return FUNCTION_SUCCEDED;
}
void  FaceList_clear(FaceList *l)
{
	elem* pnext;
	size_t i;
	for (i=0;i<l->size;++i)
	{
		pnext=l->pnext;
		l->pnext=pnext->pnext;
		free(pnext);
	}
    l->size=0;
}
FaceList*  FaceList_new()
{
	FaceList*l=malloc(sizeof(FaceList));
	if(l)
	{
	memset(l,0,sizeof(FaceList));
	}
	return l;
}
typ * FaceList_GetElement(FaceList*plist,size_t elementndex)
{
	struct elem* pelement=plist->pnext;
	int i=1;
	while(i<elementndex)
	{
		pelement=pelement->pnext;
		++i;
	}
	return &(pelement->element);
}
