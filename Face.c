

#include "Face.h"


Face* Face_new()
{
	Face *f=malloc(sizeof(Face));
	if(f)
	{
         f->vertices=VertexList_new();
         if(f->vertices)
         {
        	 return f;
         }
         else
         {
        	 free(f);
         }
	}
		return NULL;
}
void Face_free(Face** f)
{
	if(*f)
	{
		if((*f)->vertices)
		{   VertexList_clear((*f)->vertices);
			free((*f)->vertices);
			(*f)->vertices=NULL;
		}
		(*f)=NULL;
	}
}
int Face_addIndices(Face*pface,Vector3dINT vec)
{
	if(VertexList_push(pface->vertices,vec))
		return TRUE;
	else
		return FALSE;
}
