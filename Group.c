/*
 * Group.c
 *
 *  Created on: 24 lut 2017
 *      Author: Admin
 */
#include "Group.h"
#include <stdio.h>

#include "pMaterialGroupvector.h"
group* group_new(unsigned int NumOfMaterials)

	{
		group *l=malloc(sizeof(group));
		if(l)
		{
			memset(l,0,sizeof(group));
			l->faces=FaceList_new();
			if(l->faces)
			{
			  l->MaterialGroups=pMaterialGroup_vector_new();
			  if(l->MaterialGroups)
			  {

				  return l;
			  }
			  else
			  {
				  free (l->faces);
				  l->faces=NULL;
				  free(l);
				  l=NULL;
			  }

			}
			else
			{
				free(l);
				l=NULL;
			}
		}
		return l;
	}
void group_clear(group* pgroup)
{
        if(pgroup)
        {
		FaceList_clear(pgroup->faces);
		free(pgroup->faces);
		pgroup->faces=NULL;
		free(pgroup->MaterialGroups);
		pgroup->MaterialGroups=NULL;
        }
}
size_t group_isEmpty(group*pgroup)
{
	return pgroup->faces->size;
}

void SaveToOBJFile(group*pgroup,FILE*file)
{
	int i,j;
	if(pgroup->DefaultMaterial.FaceIndexBegin!=0)
	{
		i=0;
		while(i<=pgroup->DefaultMaterial.FaceIndexEnd)
		{
			VertexList *a=FaceList_GetElement(pgroup->faces,i);
			VertexList_SaveFacetoFile(a,file);


			++i;
		}
	}


   for(j=0;j<pMaterialGroup_vector_size(pgroup->MaterialGroups);++j)
   {
	   {
	   		i=0;
	   		fprintf(file,"usemtl %s",pgroup->MaterialGroups->vec[j]->Name);
	   		while(i<=pgroup->MaterialGroups->vec[j]->FaceIndexEnd)
	   		{
	   			VertexList *a=FaceList_GetElement(pgroup->faces,i);
	   			VertexList_SaveFacetoFile(a,file);


	   			++i;
	   		}
	   	}
   }
}
