/*
 * WavefrontOBJFile.c
 *
 *  Created on: 12 lut 2017
 *      Author: Admin
 */
#include "WavefrontOBJFile.h"

// declaration of static functions
static void _countElements(Model3d*,FILE*);   //call rewind(file)
static void _loadVerticesNormalsTexturevertices(Model3d*,FILE*); //call rewind(file)
static int _loadGroups(Model3d*,FILE*);//call rewind(file)
static int _init_memory(Model3d*);
static int _loadmaterials(Model3d*,FILE*);
//--------------------------------------------------------------------------------------//
int ReadWavefronOBJfromfile(Model3d* pModel,char* filename)
{
  FILE * file;


  file=fopen(filename,"r");
  if(file==NULL)
	  return FUNCTION_FAILED;

  memset(pModel,0,sizeof(Model3d));

  _countElements(pModel,file);
  _loadmaterials(pModel,file);       //loadmaterials function call always before initmemory
  if(_init_memory(pModel)==FUNCTION_FAILED)
  {
	  Model3d_clear(pModel);
	  fclose(file);
	  return FUNCTION_FAILED;

  }

  _loadVerticesNormalsTexturevertices(pModel,file);
  _loadGroups(pModel,file);


  fclose(file);
  return FUNCTION_SUCCEDED;
}


//-------------------------------------------------------------------------------------------------//
// "private" functions //
static int _loadmaterials(Model3d* pModel,FILE* file)
{   rewind(file);
    char input[MAX_LENGHT_OF_NAME];
    char c;
    int i;
    int materialindex=0;
    FILE* mtlfile;
    //look for material file path
    while(fscanf(file,"%s%c",input,&c))
    {
    	 if((strcmp(input,"mtllib"))==0)
    	 {
    		 fscanf(file,"%s%c", pModel->materiallibname,&c);
    		 break;
    	 }

    	 while(c!='\n')
    	 {
    		 c=getc(file);
    	 }
    }


    //-----------------------------------------------------------------------------------

    // change '\' in file path
    i=0;
     while( pModel->materiallibname[i]!='\0')
    {
    	if( pModel->materiallibname[i]=='\\')
    			{
    		 pModel->materiallibname[i]='\\';
    			}
    	++i;
    }


    //-----------------------------------------------------------------------------------

    mtlfile=fopen( pModel->materiallibname,"r");    //open file with materials

    if(mtlfile==NULL)
    {
    	return FUNCTION_FAILED;
    }



     while(fscanf(mtlfile,"%s%c",input,&c)>1)  // counting materials
    	 {
    	 if(strcmp(input,"newmtl")==0)
    	 {
    		 pModel->MaterialsCount++;
    	 }
               	while(c!='\n')
               	{
               		c=getc(mtlfile);
               	}
    	 }



        // alloc memory for materials
   pModel->materials=malloc((pModel->MaterialsCount) * (sizeof(Material*)));

   for(i=0;i<pModel->MaterialsCount;++i)
   {
	   pModel->materials[i]=Material_new();
	   if(pModel->materials[i]==NULL)
	   {
		   return FUNCTION_FAILED;
	   }
   }
   if(pModel->materials==NULL)
   {
	   return FUNCTION_FAILED;
   }

       //-------------------------------------------------------------------------------------------------------------
   rewind(mtlfile);
   while(fscanf(mtlfile,"%s%c",input,&c)==2)
	   {
	   if(strcmp(input,"newmtl")==0)
	   {
		   materialindex++;
		   fscanf(mtlfile,"%s",pModel->materials[materialindex-1]->name);
	   }
	   else if(strcmp(input,"Ka")==0)
	   {

		   fscanf(mtlfile,"%f%f%f",&(pModel->materials[materialindex-1]->Ambientcolor.x)
				                  ,&(pModel->materials[materialindex-1]->Ambientcolor.y)
								  ,&(pModel->materials[materialindex-1]->Ambientcolor.z));
	   }
	   else if(strcmp(input,"Kd")==0)
		   {

			   fscanf(mtlfile,"%f%f%f",&(pModel->materials[materialindex-1]->Diffusecolor.x)
					                  ,&(pModel->materials[materialindex-1]->Diffusecolor.y)
									  ,&(pModel->materials[materialindex-1]->Diffusecolor.z));
		   }
	   else if(strcmp(input,"Ks")==0)
		   {

			   fscanf(mtlfile,"%f%f%f",&(pModel->materials[materialindex-1]->Specularcolor.x)
					                  ,&(pModel->materials[materialindex-1]->Specularcolor.y)
									  ,&(pModel->materials[materialindex-1]->Specularcolor.z));
		   }
	   else if(strcmp(input,"Tr")==0)
	   		   {
	   			   fscanf(mtlfile,"%f",&(pModel->materials[materialindex-1]->transparency));
	   		   }
	   else if(strcmp(input,"f")==0)
	  	   		   {
	  	   			   fscanf(mtlfile,"%f",&(pModel->materials[materialindex-1]->transparency));
	  	   			   pModel->materials[materialindex-1]->transparency = 1-pModel->materials[materialindex-1]->transparency;
	  	   		   }


       while(c!='\n')
       {
    	   c=getc(mtlfile);
       }
	   }




return 1;
}
static void _countElements(Model3d * pModel,FILE* file)
{   char input;
	char buf[10];
	rewind(file);


	 while((input=getc(file))!=EOF)
	{
		 if(input=='v')
		 {   input=getc(file);
			 switch (input)
			 {
				 case ' ':  //vertex
						 {
							 pModel->VerticesCount++;
							 break;
						 }
				 case 'n':   // vn texture cords
						 {
							 pModel->VertexNormalsCount++;
							 break;
						 }
				 case 't':  //vt normal vertices
						 {
							 pModel->TextureVerticesCount++;
							 break;
						 }

				 default :
						 {
							 break;
						 }
								 }

		 }
		if(input=='f')
				 {
					 pModel->FacesCount++;
				 }
		if(input=='g')
						 {
			        		 input=getc(file);
			        		 if(input!='\n')
			        		 {
			        		  pModel->GroupsCount++;
			        		 }
						 }

		while(input!='\n')
		{
			input=getc(file);

		}

	}
	 if (pModel->GroupsCount==0)
	 {
		 pModel->GroupsCount++;       //if model have no groups , set one group
         pModel->NoGrups=TRUE;
	 }
}
static void _loadVerticesNormalsTexturevertices(Model3d* pModel,FILE* file)
{
	char input;
	int NumOfVertices=0
		,NumOfNormals=0
		,NumOfTextureVertices=0;

	rewind(file);
	  while((input=getc(file))!=EOF)
	  {
		  if(input=='v')
		  {
			    input=getc(file);
			  			 switch (input)
			  			 {
			  				 case ' ':  //vertex
			  						 {
			  							 fscanf(file,"%f %f %f%c",&(pModel->vertices[NumOfVertices].x)
			  									 	 	 	 	 ,&(pModel->vertices[NumOfVertices].y)
																 ,&(pModel->vertices[NumOfVertices].z)
																 ,&input);
			  							 NumOfVertices++;
			  							 break;
			  						 }
			  				 case 't':   // vp texture cords
			  						 {
			  							 fscanf(file,"%f %f %f%c",&(pModel->texture_vertices[NumOfTextureVertices].x)
			  									 	 	 	 	 ,&(pModel->texture_vertices[NumOfTextureVertices].y)
																 ,&(pModel->texture_vertices[NumOfTextureVertices].z),
																 &input);
			  							 NumOfTextureVertices++;
			  							 break;
			  						 }
			  				 case 'n':  //vt normal vertices
			  						 {
			  							 fscanf(file,"%f %f %f%c",&(pModel->vertex_normals[NumOfNormals].x)
			  									 	 	 	 	 ,&(pModel->vertex_normals[NumOfNormals].y)
																 ,&(pModel->vertex_normals[NumOfNormals].z)
																 ,&input);
			  							 NumOfNormals++;
			  							 break;
			  						 }
			  				 default :
			  						 {
			  							 break;
			  						 }

			  			 }




		  }

		while(input!='\n')
			{
				input=getc(file);

			}
	  }

}
static int _init_memory(Model3d* pModel)
{
	  pModel->texture_vertices=malloc(pModel->TextureVerticesCount * sizeof(Vector3d));
	  if(pModel->texture_vertices==NULL)
	  {
		  return FUNCTION_FAILED;
	  }
	  pModel->vertex_normals=malloc(pModel->VertexNormalsCount * sizeof(Vector3d));

	  if( pModel->vertex_normals==NULL)
	 	  {
		      return FUNCTION_FAILED;
	 	  }
	  pModel->vertices= malloc(pModel->VerticesCount * sizeof(Vector3d));

	  if(pModel->vertices==NULL)
	 	 	  {
	 	 		  return FUNCTION_FAILED;
	 	 	  }

	  pModel->groups= malloc(pModel->GroupsCount * sizeof(group*));
	  if(pModel->groups)
	  {
		  int i;
		  for(i=0;i<pModel->GroupsCount;++i)
		  {
			  pModel->groups[i]=group_new(pModel->MaterialsCount);
			  if(pModel->groups[i]==NULL)
			  {
				 			return FUNCTION_FAILED ;
			  }
		  }
	  }
	  else
	  {
		  return FUNCTION_FAILED;
	  }


	  return FUNCTION_SUCCEDED;
}

static int _loadGroups(Model3d*pModel,FILE*file)
{
	char line[MAX_LENGHT_OF_LINE];
	char input;
    int groupindex=0,CountMaterialInGroup=0;
    int i;
    int DefaultMaterial=TRUE;
    char buf[10];
    Vector3dINT vec;
    VertexList * face=NULL;

        rewind(file);

        if(pModel->NoGrups)
        {
        	groupindex=1;
        }

        size_t faceindex=0;
        while((input=getc(file))!=EOF)
			  {
         		if(input=='g')
        							 {
        				        		 input=getc(file);
        				        		 if(input!='\n')
        				        		 {
                                            if(groupindex!=0)
                                            {
                                            	if(DefaultMaterial)
                                            	{
                                            		pModel->groups[groupindex-1]->DefaultMaterial.FaceIndexBegin=1;
                                            		pModel->groups[groupindex-1]->DefaultMaterial.FaceIndexEnd=faceindex;
                                            	}
                                            	else
                                                {
                                                	 pModel->groups[groupindex-1]->MaterialGroups->vec[CountMaterialInGroup-1]->FaceIndexEnd=faceindex;
                                                }

                                            }


        				        			 fscanf(file,"%s",pModel->groups[groupindex]->groupname);
        				        			 CountMaterialInGroup=0;
        				        			 ++groupindex;
        				        			 faceindex=0;
        				        			 DefaultMaterial=TRUE;
        				        		 }
        							 }

			      if(input=='u')    //check if new material
			      {
			    	  fscanf(file,"%s",buf);
					  if (strcmp(buf, "semtl") == 0)
					  {

						  if (DefaultMaterial)
						  {
							  if (faceindex != 0)
							  {
								  pModel->groups[groupindex - 1]->DefaultMaterial.FaceIndexBegin = 1;
								  pModel->groups[groupindex - 1]->DefaultMaterial.FaceIndexEnd = faceindex;
							  }

							  DefaultMaterial = FALSE;
						  }
						  MaterialGroup* new = malloc(sizeof(MaterialGroup));
						  if (new == NULL)
						  {
							  return FUNCTION_FAILED;
						  }
						  else
						  {

							  if ((pMaterialGroup_vector_push_back(pModel->groups[groupindex - 1]->MaterialGroups, new)) == FUNCTION_FAILED)
							  {
								  free(new);
								  return FUNCTION_FAILED;
							  }
						  }

						  fscanf(file, "%s", &(new->Name));

						  if (CountMaterialInGroup == 0)
						  {

							 new->FaceIndexBegin = faceindex + 1;   //set begin of material to current index

						  }
						  else
						  {
							  pModel->groups[groupindex - 1]->MaterialGroups->vec[CountMaterialInGroup-1 ]->FaceIndexEnd = faceindex;
							  new->FaceIndexBegin = faceindex + 1;
						  }
						  ++CountMaterialInGroup;

					  }
			      }





				  if(input=='f')
				  {

					  face=VertexList_new();
	                   if(face==NULL)
	                   {
	                	   return FUNCTION_FAILED;
	                   }
					i=0;
					++faceindex;

	                fgets(line,MAX_LENGHT_OF_LINE,file);
	                while(line[i]==' ')
					{
						++i;
					}
	                while(line[i]!='\0')
	                {


	                   vec.v=0;
	                   vec.vn=0;
	                   vec.vt=0;
	                   sscanf(line+i,"%i",&vec.v);
	                   sprintf(buf,"%i",vec.v);
	                   i+=strlen(buf);
                       if(line[i]=='/')     //    possible formats v , v/vt ,v/vt/vn , v//vn
                       {
                    	   ++i;
                    	   if(line[i]!='/')    //v/vt...
                    	   {
                    		   sscanf(line+i,"%i",&vec.vt);
                    		   sprintf(buf,"%i",vec.vt);
                    		   i+=strlen(buf);
                    	   }
                    	   else        // v//vn
                    	   {
                    		   ++i;
                    		   sscanf(line+i,"%i",&vec.vn);
                    		   sprintf(buf,"%i",vec.vn);
                    		   i+=strlen(buf);
                    	   }
                    	   if(line[i]=='/')   // v/vt/vn
                    	   {
                    		   ++i;
                    		   sscanf(line+i,"%i",&vec.vn);
                    		   sprintf(buf,"%i",vec.vn);
                    		   i+=strlen(buf);
                    	   }

                       }
                       ++i;
                       VertexList_push(face,vec);
	                }

	                FaceList_push(pModel->groups[groupindex-1]->faces,*face);
	                free(face);
	                face=NULL;
	                input='\n';
				  }


				while(input!='\n')
					{
						input=getc(file);

					}
			  }
        if(DefaultMaterial)
        {

        	pModel->groups[groupindex-1]->DefaultMaterial.FaceIndexEnd=faceindex;
        }
        else
        {
        	 pModel->groups[groupindex-1]->MaterialGroups->vec[CountMaterialInGroup-1]->FaceIndexEnd=faceindex;
        }

return FUNCTION_SUCCEDED;
}

int SaveWavefrontOBJ(Model3d*pModel,char* name)
{   int i;
	FILE *file = fopen(name, "wb");

	fprintf(file,"mtllib %s \n",pModel->materiallibname);

	for(i=0;i<pModel->VerticesCount;++i)
	{
		fprintf(file,"v %f %f %f \n",pModel->vertices[i].x
								 ,pModel->vertices[i].y
								 ,pModel->vertices[i].z);
	}
	for(i=0;i<pModel->VertexNormalsCount;++i)
		{
			fprintf(file,"vn %f %f %f \n",pModel->vertex_normals[i].x
									 	 ,pModel->vertex_normals[i].y
										 ,pModel->vertex_normals[i].z);
		}
	for(i=0;i<pModel->TextureVerticesCount;++i)
			{
				fprintf(file,"vt %f %f %f \n",pModel->texture_vertices[i].x
										 	 ,pModel->texture_vertices[i].y
											 ,pModel->texture_vertices[i].z);
			}

	for(i=0;i<pModel->GroupsCount;i++)
	{
		fprintf(file,"g %s\n",pModel->groups[i]->groupname);
		SaveToOBJFile(pModel->groups[i],file);
	}



	fclose(file);
	return 1;
}

















