/*
 * MaterialGroup.h
 *
 *  Created on: 08.03.2017
 *      Author: Cyprian
 */
#include "defines.h"
#ifndef MATERIALGROUP_H_
#define MATERIALGROUP_H_
typedef struct
{
	char Name[MAX_LENGHT_OF_NAME];
	unsigned int FaceIndexBegin,FaceIndexEnd;
}MaterialGroup;

#endif /* MATERIALGROUP_H_ */
