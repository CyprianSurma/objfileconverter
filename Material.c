/*
 * Material.c
 *
 *  Created on: 26 lut 2017
 *      Author: Admin
 */
#include "Material.h"
#include "stdlib.h"
#include "string.h"
Material* Material_new()
{
	Material* pmaterial=malloc(sizeof(Material));
	if(pmaterial)
	{
		memset(pmaterial,0,sizeof(Material));
        pmaterial->map_Ka[0]='\0';
        pmaterial->map_Kd[0]='\0';
        pmaterial->map_Ks[0]='\0';
        pmaterial->map_Ns[0]='\0';
        pmaterial->map_d[0]='\0';
        pmaterial->map_bump[0]='\0';

	}
	return pmaterial;
}
