/*
 * Group.h
 *
 *  Created on: 22 lut 2017
 *      Author: Admin
 */
#include "defines.h"
#include "FaceList.h"
#include "MaterialGroup.h"
#include <stdio.h>

#include "pMaterialGroupvector.h"
#ifndef GROUP_H_
#define GROUP_H_

typedef struct
{


	char groupname[MAX_LENGHT_OF_NAME];
    FaceList* faces;
    pMaterialGroup_vector* MaterialGroups;
    MaterialGroup DefaultMaterial;
}group;

group* group_new(unsigned int);
void group_clear(group*);
size_t group_isEmpty(group*);
void group_addface();
void SaveToOBJFile(group*,FILE*);
#endif /* GROUP_H_ */
